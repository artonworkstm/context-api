import React, { Component, createContext} from "react";

export const ThemeContext = createContext();

class ThemeContextProvider extends Component {
    state = { 
        themeMode: 'dark',
        light: {
            textColor: "#000",
            backgroundColor: "#fff"
        },
        dark: {
            textColor: "#fff",
            backgroundColor: "#333"
        }
    }
    toggleTheme = () => {
        this.setState({ themeMode: this.state.themeMode === 'dark' ? 'light' : 'dark' })
    }
    render() { 
        return ( 
            <ThemeContext.Provider value={{...this.state, toggleTheme: this.toggleTheme}}>
                {this.props.children}
            </ThemeContext.Provider>
         );
    }
}
 
export default ThemeContextProvider;