//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { ThemeContext } from '../contexts/ThemeContext';

// create a component
class ThemeToggler extends Component {

    static contextType = ThemeContext

    render() {
        const { themeMode, light, dark, toggleTheme } = this.context;
        return (
            <TouchableOpacity onPress={() => {toggleTheme()}}>
                <View>
                    <Text style={{fontSize: 32}}>{themeMode === 'light' ? "🌙" : "☀️"}</Text>
                </View>
            </TouchableOpacity>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default ThemeToggler;
