//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { ThemeContext } from '../contexts/ThemeContext';
import ThemeToggler from '../components/ThemeToggler';

// create a component
class HomeScreen extends Component {

    static contextType = ThemeContext

    render() {
        const { themeMode, light, dark } = this.context
        const theme = themeMode === 'light' ? light : dark;
        return (
            <View style={[styles.container, {backgroundColor: theme.backgroundColor}]}>
              <View style={{ alignItems: 'center' }}>
                <Text style={[{fontSize: 32, fontWeight: '700', marginBottom: '10%'}, {color: theme.textColor}]}>Theme app</Text>
              </View>
              <ThemeToggler />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: '10%',
    },
});

//make this component available to the app
export default HomeScreen;
