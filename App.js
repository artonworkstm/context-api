//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import ThemeToggler from './components/ThemeToggler';
import ThemeContextProvider from './contexts/ThemeContext';
import HomeScreen from './screens/HomeScreen';

// create a component
class App extends Component {
  render() {
    return (
      <ThemeContextProvider>
        <HomeScreen />
      </ThemeContextProvider>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#2c3e50',
  },
});

//make this component available to the app
export default App;
